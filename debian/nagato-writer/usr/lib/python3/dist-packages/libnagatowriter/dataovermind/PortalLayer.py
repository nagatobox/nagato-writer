
from libnagato.Object import NagatoObject
from libnagato.object.Transmitter import NagatoTransmitter
from libnagatowriter.dataovermind.ConversionLayer import NagatoConversionLayer


class NagatoPortalLayer(NagatoObject):

    def _yuki_n_converted_to_html(self, uri):
        yuki_data = "converted_to_html", uri
        self._transmitter.transmit(yuki_data)

    def _yuki_n_heading_moved(self, heading_id):
        yuki_data = "heading_moved", heading_id
        self._transmitter.transmit(yuki_data)

    def _yuki_n_register_preview_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = NagatoTransmitter(self)
        NagatoConversionLayer(self)
