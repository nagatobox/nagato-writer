
from libnagato.menu.Action import NagatoActionCore


class NagatoList(NagatoActionCore):

    def _on_activate(self, widget):
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_start, yuki_end = yuki_buffer.get_selection_bounds()
        yuki_text = yuki_start.get_text(yuki_end)
        yuki_text = "+ "+yuki_text.replace("\n", "\n+ ")
        yuki_buffer.delete(yuki_start, yuki_end)
        yuki_buffer.insert_at_cursor(yuki_text, -1)

    def _on_map(self, widget):
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_selection = yuki_buffer.get_selection_bounds()
        self.set_sensitive(len(yuki_selection) > 1)

    def _initialize_variables(self):
        self._title = "List"
