
from libnagato.menu.Action import NagatoActionCore


class NagatoNumberedList(NagatoActionCore):

    def _get_replacement(self, start_iter, end_iter):
        yuki_text = start_iter.get_text(end_iter)
        yuki_lines = yuki_text.split("\n")
        for yuki_index in range(0, len(yuki_lines)):
            yuki_line_text = yuki_lines[yuki_index]
            yuki_new_text = "{}. {}".format(yuki_index+1, yuki_line_text)
            yuki_lines[yuki_index] = yuki_new_text
        return "\n".join(yuki_lines)

    def _on_activate(self, widget):
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_start, yuki_end = yuki_buffer.get_selection_bounds()
        yuki_text = self._get_replacement(yuki_start, yuki_end)
        yuki_buffer.delete(yuki_start, yuki_end)
        yuki_buffer.insert_at_cursor(yuki_text, -1)

    def _on_map(self, widget):
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_selection = yuki_buffer.get_selection_bounds()
        self.set_sensitive(len(yuki_selection) > 1)

    def _initialize_variables(self):
        self._title = "Numbered List"
