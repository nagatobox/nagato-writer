
from libnagato.menu.Action import NagatoActionCore
from gi.repository import Gio
from gi.repository import GLib

FLAG_OUT = Gio.SubprocessFlags.STDOUT_PIPE
FLAG_IN = Gio.SubprocessFlags.STDIN_PIPE

from libnagato.dialog.chooser.Save import NagatoSave


class NagatoPdf(NagatoActionCore):

    def _on_cat_finished(self, subprocess, task, destination):
        yuki_success, yuki_out, _ = subprocess.communicate_finish(task)
        if not yuki_success:
            return
        yuki_command = ["wkhtmltopdf", "-", destination]
        yuki_subprocess = Gio.Subprocess.new(yuki_command, FLAG_IN)
        yuki_subprocess.communicate(yuki_out, None)

    def _on_activate(self, widget):
        yuki_uri = self._enquiry("YUKI.N > uri")
        if yuki_uri is None:
            return
        yuki_destination = NagatoSave.call(current_name="name_it.pdf")
        if yuki_destination is None:
            return
        yuki_path, _ = GLib.filename_from_uri(yuki_uri)
        yuki_subprocess = Gio.Subprocess.new(["cat", yuki_path], FLAG_OUT)
        yuki_subprocess.communicate_async(
            None,
            None,
            self._on_cat_finished,
            yuki_destination
            )

    def _on_map(self, widget):
        yuki_uri = self._enquiry("YUKI.N > uri")
        self.set_sensitive(yuki_uri is not None)

    def _initialize_variables(self):
        self._title = "PDF"

