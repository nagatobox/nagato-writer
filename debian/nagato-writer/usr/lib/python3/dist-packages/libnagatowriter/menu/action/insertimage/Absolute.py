
from libnagato.menu.Action import NagatoActionCore
from libnagato.dialog.chooser.Load import NagatoLoad


class NagatoAbsolute(NagatoActionCore):

    def _on_activate(self, widget):
        yuki_response = NagatoLoad.call(mime_type="image/*")
        if yuki_response is None:
            return
        yuki_text = "![nagato-img]({})".format(yuki_response)
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_buffer.insert_at_cursor(yuki_text, -1)

    def _on_map(self, widget):
        pass

    def _initialize_variables(self):
        self._title = "Insert Image (Absolute)"
