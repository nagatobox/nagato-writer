
from libnagato.menu.Action import NagatoActionCore
from libnagato.dialog.chooser.Load import NagatoLoad
from libnagatowriter.util.AbsoluteToRelative import NagatoAbsoluteToRelative


class NagatoRelative(NagatoActionCore):

    def _on_activate(self, widget):
        yuki_response = NagatoLoad.call(mime_type="image/*")
        if yuki_response is None:
            return
        yuki_relative_path = self._absolute_to_relative(yuki_response)
        yuki_text = "![nagato-img]({})".format(yuki_relative_path)
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_buffer.insert_at_cursor(yuki_text, -1)

    def _on_map(self, widget):
        yuki_path = self._enquiry("YUKI.N > source path", 0)
        self.set_sensitive(yuki_path is not None)

    def _initialize_variables(self):
        self._title = "Insert Image (Relative)"
        self._absolute_to_relative = NagatoAbsoluteToRelative(self)
