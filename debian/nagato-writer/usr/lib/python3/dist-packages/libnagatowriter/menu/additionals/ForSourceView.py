
from libnagatowriter.menu.action.insertimage.Absolute import NagatoAbsolute
from libnagatowriter.menu.action.insertimage.Relative import NagatoRelative
from libnagatowriter.menu.action.List import NagatoList
from libnagatowriter.menu.action.NumberedList import NagatoNumberedList
from libnagato.menu.Separator import NagatoSeparator


class AsakuraAdditionals(object):

    def __init__(self, root_menu):
        NagatoSeparator(root_menu)
        NagatoAbsolute(root_menu)
        NagatoRelative(root_menu)
        NagatoList(root_menu)
        NagatoNumberedList(root_menu)
