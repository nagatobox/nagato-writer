
from libnagato.menu.Sub import NagatoSubCore
from libnagatowriter.menu.action.export.Pdf import NagatoPdf


class NagatoExport(NagatoSubCore):

    def _initialize_child_menus(self):
        NagatoPdf(self)

    def _set_variables(self):
        self._title = "Export"
