
from gi.repository import GLib
from libnagato.Object import NagatoObject

COMMAND = ["pandoc", "-f", "markdown_github", "-t", "html5", "-s"]


class NagatoCommand(NagatoObject):

    def _get_css_uri(self):
        yuki_data = "preview", "theme"
        yuki_theme_name = self._enquiry("YUKI.N > config", yuki_data)
        yuki_css = "css/{}_theme.css".format(yuki_theme_name)
        yuki_path = self._enquiry("YUKI.N > resources", yuki_css)
        return GLib.filename_to_uri(yuki_path)

    def get(self):
        yuki_source_path = self._enquiry("YUKI.N > source path")
        yuki_target_path = self._enquiry("YUKI.N > target path")
        yuki_basename = GLib.filename_display_basename(yuki_source_path)
        yuki_command = COMMAND.copy()
        yuki_command.extend(["-c", self._get_css_uri()])
        yuki_command.extend(["-T", yuki_basename])
        yuki_command.extend([yuki_source_path])
        yuki_command.extend(["-o", yuki_target_path])
        return yuki_command

    def __init__(self, parent):
        self._parent = parent
