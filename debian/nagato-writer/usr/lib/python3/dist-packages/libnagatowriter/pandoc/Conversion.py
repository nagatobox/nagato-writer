
from libnagato.Object import NagatoObject
from libnagatowriter.pandoc.Subprocess import NagatoSubprocess
from libnagatowriter.pandoc.WorkingFile import NagatoWorkingFile


class NagatoConversion(NagatoObject):

    def _inform_target_path(self):
        return self._working_file.path

    def _yuki_n_conversion_finished(self):
        self._working_file.solve()
        self._raise("YUKI.N > converted to html", self._working_file.uri)

    def convert(self):
        self._pandoc_subprocess.run()

    def __init__(self, parent):
        self._parent = parent
        self._pandoc_subprocess = NagatoSubprocess(self)
        self._working_file = NagatoWorkingFile(self)
