
from lxml import etree
from gi.repository import Gio
from libnagato.Object import NagatoObject
from libnagatowriter.pandoc.postconversion.Image import NagatoImage
from libnagatowriter.pandoc.postconversion.Heading import NagatoHeading


class NagatoWorkingFile(NagatoObject):

    def solve(self):
        yuki_parser = etree.HTMLParser()
        yuki_tree = etree.parse(self._tmp_file.get_path(), parser=yuki_parser)
        self._heading.solve(yuki_tree.getroot())
        self._image.solve(yuki_tree.getroot())
        yuki_tree.write(self._tmp_file.get_path())

    def __init__(self, parent):
        self._parent = parent
        self._tmp_file, _ = Gio.File.new_tmp()
        self._heading = NagatoHeading(self)
        self._image = NagatoImage(self)

    @property
    def uri(self):
        return self._tmp_file.get_uri()

    @property
    def path(self):
        return self._tmp_file.get_path()
