
from libnagato.Object import NagatoObject
from lxml import etree

HEADINGS = ["h1", "h2", "h3", "h4", "h5", "h6", "h7"]


class NagatoHeading(NagatoObject):

    def solve(self, root):
        yuki_index = 0
        for yuki_element in root.iter():
            if yuki_element.tag not in HEADINGS:
                continue
            yuki_element.attrib["id"] = "nagato-heading-"+str(yuki_index)
            yuki_index += 1
        yuki_id = "nagato-heading-last"
        root.insert(3, etree.Element(yuki_id, tag="H1", id=yuki_id))

    def __init__(self, parent):
        self._parent = parent
