
from gi.repository import GLib
from libnagato.Object import NagatoObject
from libnagatowriter.util.RelativeToAbsolute import NagatoRelativeToAbsolute


class NagatoImage(NagatoObject):

    def _embed_base64(self, element, src):
        try:
            yuki_file_name, _ = GLib.filename_from_uri("file://"+src)
            _, yuki_byte = GLib.file_get_contents(yuki_file_name)
            yuki_base64 = GLib.base64_encode(yuki_byte)
            yuki_format = "data:image/jpeg;base64,{}"
            element.attrib["src"] = yuki_format.format(yuki_base64)
        except GLib.Error as yuki_error:
            print("YUKI.N > exception occured:", yuki_error)
            return

    def solve(self, root):
        for yuki_element in root.iter("img"):
            yuki_uri = yuki_element.attrib["src"]
            yuki_src = self._relative_to_absolute(yuki_uri)
            self._embed_base64(yuki_element, yuki_src)

    def __init__(self, parent):
        self._parent = parent
        self._relative_to_absolute = NagatoRelativeToAbsolute(self)
