
from gi.repository import Gtk
from libnagato.Object import NagatoObject
from libnagato.Ux import Unit
from libnagatowriter.ui.Paned import NagatoPaned


class NagatoGrid(Gtk.Grid, NagatoObject):

    def _yuki_n_attach_to_grid(self, user_data):
        yuki_widget, yuki_geometries = user_data
        self.attach(yuki_widget, *yuki_geometries)

    def _initialize_grid(self):
        Gtk.Grid.__init__(self)
        self.set_border_width(Unit("grid-spacing"))
        self.set_row_spacing(Unit("grid-spacing"))
        self.set_column_spacing(Unit("grid-spacing"))
        self.set_column_homogeneous(True)

    def __init__(self, parent):
        self._parent = parent
        self._initialize_grid()
        NagatoPaned(self)
        self._raise("YUKI.N > add to event box", self)
