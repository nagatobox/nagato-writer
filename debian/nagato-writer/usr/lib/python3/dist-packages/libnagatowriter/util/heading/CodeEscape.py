

class HaruhiCodeEscape:

    def is_heading(self, line):
        if line.startswith("```"):
            self._code_escape = not self._code_escape
        return line.startswith("#") and not self._code_escape

    def reset(self):
        self._code_escape = False

    def __init__(self):
        self._code_escape = False
