
from libnagato.Object import NagatoObject


class NagatoName(NagatoObject):

    def set_data(self, line_number):
        yuki_data = line_number, "nagato-heading-"+str(self._index)
        self._raise("YUKI.N > heading data", yuki_data)
        self._index += 1

    def reset(self):
        self._index = 0

    def __init__(self, parent):
        self._parent = parent
        self._index = 0
