
from libnagato.Object import NagatoObject
from libnagatowriter.util.heading.Reader import NagatoReader
from libnagatowriter.util.heading.Cache import NagatoCache


class NagatoParser(NagatoObject):

    def _yuki_n_heading_data(self, user_data):
        self._cache.set_data(user_data)

    def _yuki_n_parse_finished(self, last_line_number):
        self._last_line_number = last_line_number+1

    def parse(self, path=None):
        self._cache.clear()
        self._reader.read(path)

    def move(self, line_number):
        if line_number+5 > self._last_line_number:
            self._raise("YUKI.N > heading moved", "nagato-heading-last")
        else:
            self._cache.move(line_number)

    def __init__(self, parent):
        self._parent = parent
        self._last_line_number = 0
        self._cache = NagatoCache(self)
        self._reader = NagatoReader(self)
