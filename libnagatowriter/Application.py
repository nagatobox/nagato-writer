
from libnagato.application.Application import NagatoApplication as TFEI
from libnagato.config.CssMainImage import NagatoCssMainImage
from libnagato.config.CssMenuImage import NagatoCssMenuImage
from libnagato.config.CssFont import NagatoCssFont
from libnagato.config.CssOpacity import NagatoCssOpacity
from libnagatowriter.ui.MainWindow import NagatoMainWindow


class NagatoApplication(TFEI):

    def _yuki_n_register_config_object(self, config_object):
        self._config_objects.append(config_object)

    def _initialize_css_replacements(self):
        self._append_css_replacement(NagatoCssMainImage(self))
        self._append_css_replacement(NagatoCssMenuImage(self))
        self._append_css_replacement(NagatoCssFont(self))
        self._append_css_replacement(NagatoCssOpacity(self))

    def run(self):
        self._config_objects = []
        NagatoMainWindow(self)
