
from gi.repository import Gio
from libnagato.Object import NagatoObject


class NagatoSourceFile(NagatoObject):

    def _get_parent_directory(self, depth):
        yuki_depth = depth
        yuki_file = self._gio_file
        while yuki_depth > 0:
            yuki_file = yuki_file.get_parent()
            if yuki_file is None:
                return None
            yuki_depth -= 1
        return yuki_file.get_path()

    def get_path(self, depth):
        if "_gio_file" not in dir(self):
            return None
        return self._get_parent_directory(depth)

    def set_path(self, path):
        self._gio_file = Gio.File.new_for_path(path)

    def __init__(self, parent):
        self._parent = parent
