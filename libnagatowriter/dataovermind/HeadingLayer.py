
from libnagato.Object import NagatoObject
from libnagatowriter.util.heading.Parser import NagatoParser
from libnagatowriter.eventbox.ForGrid import NagatoEventBox


class NagatoHeadingLayer(NagatoObject):

    def _yuki_n_new_file(self, path=None):
        if path is not None:
            self._heading_parser.parse(path)
        self._raise("YUKI.N > new file", path)

    def _yuki_n_cursor_moved(self, user_data):
        yuki_lines, yuki_line_number, yuki_line_offset = user_data
        self._heading_parser.move(yuki_line_number)

    def __init__(self, parent):
        self._parent = parent
        self._heading_parser = NagatoParser(self)
        NagatoEventBox(self)
