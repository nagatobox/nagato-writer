
from libnagato.ui.EventBox import NagatoEventBox as TFEI
from libnagatowriter.ui.Grid import NagatoGrid
from libnagatowriter.menu.context.ForChrome import NagatoContextMenu


class NagatoEventBox(TFEI):

    def _yuki_n_add_to_event_box(self, widget):
        self.add(widget)

    def _on_initialize(self):
        NagatoContextMenu(self)
        self._grid = NagatoGrid(self)
        self._raise("YUKI.N > add to main window", self)
