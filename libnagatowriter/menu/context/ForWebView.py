
from libnagato.menu.Item import NagatoItem
from libnagato.menu.Separator import NagatoSeparator
from libnagato.menu.Context import NagatoContextCore
from libnagatowriter.menu.sub.Export import NagatoExport
from libnagatowriter.menu.sub.SelectCss import NagatoSelectCss


class NagatoContextMenu(NagatoContextCore):

    def _inform_hit_test_result(self):
        return self._hit_test_result

    def _on_context_menu(self, web_view, context_menu, event, hit_test_result):
        # this method simply returns True to abort default context menu.
        return True

    def _on_target_changed(self, web_view, hit_test_result, modifiers):
        self._hit_test_result = hit_test_result

    def _add_gtk_callbacks(self):
        self._parent.connect("context-menu", self._on_context_menu)
        self._parent.connect("mouse-target-changed", self._on_target_changed)

    def _initialize_children(self):
        self._hit_test_result = None
        NagatoExport(self)
        NagatoSelectCss(self)
        NagatoSeparator(self)
        NagatoItem(self, "About", "YUKI.N > about")
        NagatoItem(self, "Quit", "YUKI.N > quit")
