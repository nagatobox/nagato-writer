
from libnagato.menu.Item import NagatoItem
from libnagato.menu.Sub import NagatoSubCore

MESSAGE = "YUKI.N > config"


class NagatoSelectCss(NagatoSubCore):

    def _initialize_child_menus(self):
        NagatoItem(self, "dark", MESSAGE, ("preview", "theme", "dark"))
        NagatoItem(self, "github", MESSAGE, ("preview", "theme", "github"))
        NagatoItem(self, "light", MESSAGE, ("preview", "theme", "light"))
        NagatoItem(self, "mint", MESSAGE, ("preview", "theme", "mint"))
        NagatoItem(self, "nagato", MESSAGE, ("preview", "theme", "nagato"))
        NagatoItem(self, "ocean", MESSAGE, ("preview", "theme", "ocean"))
        NagatoItem(self, "sakura", MESSAGE, ("preview", "theme", "sakura"))

    def _set_variables(self):
        self._title = "Select Css"
