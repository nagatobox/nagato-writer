
from libnagato.Object import NagatoObject
from libnagatowriter.pandoc.Conversion import NagatoConversion
from libnagatowriter.data.SourceFile import NagatoSourceFile
from libnagatowriter.ui.MainWindow import NagatoMainWindow


class NagatoConversionLayer(NagatoObject):

    def _inform_source_path(self, depth=0):
        return self._source_file.get_path(depth)

    def _yuki_n_new_file(self, file_name):
        self._source_file.set_path(file_name)
        self._pandoc.convert()

    def __init__(self, parent):
        self._parent = parent
        self._source_file = NagatoSourceFile(self)
        self._pandoc = NagatoConversion(self)
        NagatoMainWindow(self)
