
from gi.repository import Gio
from libnagato.Object import NagatoObject
from libnagatowriter.pandoc.Command import NagatoCommand

FLAG = Gio.SubprocessFlags.NONE


class NagatoSubprocess(NagatoObject):

    def run(self):
        yuki_subprocess = Gio.Subprocess.new(self._command.get(), FLAG)
        yuki_success, _, _ = yuki_subprocess.communicate(None, None)
        if yuki_success:
            self._raise("YUKI.N > conversion finished")

    def change_config(self):
        if self._enquiry("YUKI.N > source path") is not None:
            self.run()

    def __init__(self, parent):
        self._parent = parent
        self._command = NagatoCommand(self)
        self._raise("YUKI.N > register config object", self)
