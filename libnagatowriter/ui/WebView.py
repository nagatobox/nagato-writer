
from gi.repository import WebKit2
from libnagato.Object import NagatoObject
from libnagato.util import CssProvider
from libnagatowriter.menu.context.ForWebView import NagatoContextMenu


class NagatoWebView(WebKit2.WebView, NagatoObject):

    def _converted_to_html(self, uri):
        self._base_uri = uri
        self.load_uri(uri)

    def _heading_moved(self, heading_id):
        if "_base_uri" in dir(self):
            self.load_uri(self._base_uri+"#"+heading_id)

    def _inform_uri(self):
        return self.get_uri()

    def receive_transmission(self, user_data):
        yuki_task, yuki_data = user_data
        yuki_method = getattr(self, "_"+yuki_task)
        yuki_method(yuki_data)

    def __init__(self, parent):
        self._parent = parent
        WebKit2.WebView.__init__(self)
        self.load_uri("this is meaningless uri to show void page.")
        CssProvider.set_to_widget(self, "preview")
        self._raise("YUKI.N > register preview object", self)
        NagatoContextMenu(self)
