
from gi.repository import Gio
from libnagato.Object import NagatoObject


class NagatoAbsoluteToRelative(NagatoObject):

    def _get_depth(self, path):
        yuki_depth = 1
        while True:
            yuki_dir = self._enquiry("YUKI.N > source path", yuki_depth)
            if path.startswith(yuki_dir):
                return yuki_depth, yuki_dir
            yuki_depth += 1

    def __call__(self, path):
        yuki_depth, yuki_parent = self._get_depth(path)
        yuki_file_parent = Gio.File.new_for_path(yuki_parent)
        yuki_relative_base = yuki_file_parent.get_relative_path(
            Gio.File.new_for_path(path)
            )
        yuki_relative_parent = "../"*(yuki_depth-1)
        return yuki_relative_parent+yuki_relative_base

    def __init__(self, parent):
        self._parent = parent
