
from libnagato.Object import NagatoObject
from libnagatowriter.util.directoryfetcher.Current import NagatoCurrent
from libnagatowriter.util.directoryfetcher.Parent import NagatoParent


class NagatoRelativeToAbsolute(NagatoObject):

    def _is_absolute(self, src):
        if src.startswith("/") or src.startswith("http"):
            return True
        return False

    def __call__(self, path):
        if self._is_absolute(path):
            return path
        yuki_path = self._current_directory_fetcher.solve(path)
        return self._parent_directory_fetcher.solve(yuki_path)

    def __init__(self, parent):
        self._parent = parent
        self._current_directory_fetcher = NagatoCurrent(self)
        self._parent_directory_fetcher = NagatoParent(self)
