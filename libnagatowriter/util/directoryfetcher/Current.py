
from libnagato.Object import NagatoObject


class NagatoCurrent(NagatoObject):

    def solve(self, src):
        yuki_directory = self._enquiry("YUKI.N > source path", 1)
        if not src.startswith("."):
            return yuki_directory+"/"+src
        elif src.startswith("./"):
            return yuki_directory+src[1:]
        return src

    def __init__(self, parent):
        self._parent = parent
