
from libnagato.Object import NagatoObject


class NagatoParent(NagatoObject):

    def _get_depth(self, src):
        yuki_src = src
        yuki_depth = 1
        while yuki_src.startswith("../"):
            yuki_src = yuki_src[3:]
            yuki_depth += 1
        return yuki_depth, yuki_src

    def solve(self, src):
        if not src.startswith("../"):
            return src
        yuki_depth, yuki_base_path = self._get_depth(src)
        yuki_parent = self._enquiry("YUKI.N > source path", yuki_depth)
        return yuki_parent+"/"+yuki_base_path

    def __init__(self, parent):
        self._parent = parent
