
from libnagato.Object import NagatoObject


class NagatoCache(NagatoObject):

    def move(self, line_number):
        yuki_heading_cached = "nagato-heading-0"
        for yuki_line_number, yuki_heading_id in self._data.items():
            if yuki_line_number > line_number:
                break
            yuki_heading_cached = yuki_heading_id
        self._raise("YUKI.N > heading moved", yuki_heading_cached)

    def has_data(self):
        return len(self._data) > 0

    def clear(self):
        self._data.clear()

    def set_data(self, user_data):
        yuki_line_number, yuki_heading_data = user_data
        self._data[yuki_line_number] = yuki_heading_data

    def __init__(self, parent):
        self._parent = parent
        self._data = {}
