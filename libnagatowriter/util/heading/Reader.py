
from libnagato.Object import NagatoObject
from libnagatowriter.util.heading.Name import NagatoName
from libnagatowriter.util.heading.LineReader import HaruhiLineReader
from libnagatowriter.util.heading.CodeEscape import HaruhiCodeEscape


class NagatoReader(NagatoObject):

    def read(self, path=None):
        self._name.reset()
        self._code_escape.reset()
        yuki_line_number = 0
        for yuki_line in self._line_reader.read(path):
            yuki_line_number += 1
            if self._code_escape.is_heading(yuki_line):
                self._name.set_data(yuki_line_number)
        self._raise("YUKI.N > parse finished", yuki_line_number)

    def __init__(self, parent):
        self._parent = parent
        self._line_reader = HaruhiLineReader()
        self._name = NagatoName(self)
        self._code_escape = HaruhiCodeEscape()
